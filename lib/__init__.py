"""
# This file controls which input names call which functions.
"""
from collections import OrderedDict
from lib import stats, diff, norm, merge, units

operations_dict = OrderedDict()

operations_dict["Stats"] = OrderedDict()
operations_dict["Stats"]["sastat"] = stats.sample_cli
operations_dict["Stats"]["grstat"] = stats.genomic_region
operations_dict["Stats"]["esstat"] = stats.editing_site

operations_dict["Stats"]["swoe"] = stats.sample_wise_overall_editing
operations_dict["Stats"]["rwoe"] = stats.region_wise_overall_editing

# Unit
operations_dict["Units"] = OrderedDict()
operations_dict["Units"]["sample-epk"] = units.epk_sample_wise
operations_dict["Units"]["region-epk"] = units.epk_region_wise

operations_dict["Differential Editing"] = OrderedDict()
operations_dict["Differential Editing"]["sadiff"] = diff.sample
operations_dict["Differential Editing"]["grdiff"] = diff.region_diff
operations_dict["Differential Editing"]["eidiff"] = diff.editing_site_diff

operations_dict["Normalization"] = OrderedDict()
operations_dict["Normalization"]["sanorm"] = norm.sample
operations_dict["Normalization"]["grnorm"] = norm.genomic_region

operations_dict["Merge"] = OrderedDict()
operations_dict["Merge"]["islands"] = merge.find_islands
operations_dict["Merge"]["esmerge"] = merge.merge_editing_sites
operations_dict["Merge"]["eimerge"] = merge.merge_editing_islands

'''
operations_dict = {
    "Stats": {
        "grstat": stats.genomic_region,
        "sastat": stats.sample_cli,
        "search": stats.search,
    },
    "Differential Editing": {
        "sadiff": diff.sample,
        "grdiff": diff.region_diff,
        "eidiff": diff.editing_site_diff
    },
    "Editing Normalization": {
        # "esnorm": norm.editing_site,
        "sanorm": norm.sample,
        "grnorm": norm.genomic_region,
    },
    "Merge": {
        "esmerge": merge.merge_editing_sites,
        "eimerge": merge.merge_editing_islands,
        "islands": merge.find_islands,
        "split": merge.split
    }
}
'''
