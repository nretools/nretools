from lib.parsers import generate_snvs
from collections import namedtuple
from random import randint, random
from lib.io import shared_params
from lib.shared import passes_min_coverage, passes_min_editing, get_strand, base_transition_tuples_and_titles, passes_max_percent_editing
from pysam import AlignmentFile, Fastafile
from statistics import mean, stdev
from lib.parsers import generate_snvs, coverage_depth, find_numbers_of_ref_and_alt_reads
from lib.parsers import SampleGroup
from scipy.stats import ttest_ind, combine_pvalues, norm

FILE_DELIMITER = ","

import warnings
warnings.filterwarnings("ignore")

r_script = """

dataFrame1 <- data.frame(
    siteEPK=c(100,105,110,114,56,73,0,110),
    repAvgEPK=c(50,51,52,53,27,26,25,38),
    size=c(100,101,102,103,104,105,106,107),
    depth=c(5,5.1,5.2,5.3,5.4,5.5,5.6,5.7),
    group=c(rep("A",4),rep("B",4))
)

minVal<-min(dataFrame1$siteEPK[dataFrame1$siteEPK>0])/2
dataFrame1$siteEPK[dataFrame1$siteEPK<=0] <- minVal
dataFrame1$logSiteEPK                     <- log(dataFrame1$siteEPK)
dataFrame1$logRepAvgEPK                   <- log(dataFrame1$repAvgEPK)

lm1 <- lm(logSiteEPK~logRepAvgEPK+size+depth+group,data=dataFrame1)

pValue <- summary(lm1)$coefficients['groupB','Pr(>|t|)']

"""


def make_groups(dict_of_groups, labels=None):
    """
    { "bam": ["bam1,bam2", "bam3,bam4"]
    {
        "group_1": {"vcf":[vcf_1, vcf_2], "bam":[bam_1, bam_2]}
        "group_2": {"vcf":[vcf_3, vcf_4], "bam":[bam_3, bam_4]}
    }

    :param dict_of_groups:
    :return:
    """

    sorted_keys = sorted(dict_of_groups)

    if labels:
        labels = ",".split(FILE_DELIMITER)
    else:
        first_label_groups = len(dict_of_groups[sorted_keys[0]])
        labels = []
        for i in range(1, first_label_groups+1):
            labels.append("Group_%s" % i)

    group_names = []
    for dict_key in sorted(dict_of_groups):
        group_names.append(dict_key)

    print(group_names)

    GroupObj = namedtuple('GroupObj', group_names)

    g = GroupObj(["1", "3"], ["2", "4"])
    print(g)
    print(GroupObj)

    groups = []
    for i in range(len(labels)):
        tmp_list = []
        for sorted_key in sorted_keys:
            tmp_list.append(dict_of_groups[sorted_key][i].split(","))

        print(tmp_list)

        groups.append(GroupObj(*tmp_list))
    return groups


def build_grouped_sample_data_structure(list_of_grouped_samples, delimiter=","):
    """ Build an ordered nested data structure of groups with samples files contained within.

    :param list_of_grouped_samples:
    :param delimiter:
    :return:
    """
    return [group.split(delimiter) for group in list_of_grouped_samples]


def site_level_differential_editing_decision_function(group_1, group_2, coverage_1, coverage_2):

    choices = ["YES", "NO", "NO_TEST"]

    return choices[randint(0, 2)], random()


def find_total_area_covered(n, file_name):
    total_area = 0
    for line in open(file_name):
        sl = line.split()
        if sl[0] == "genome" and int(sl[1]) >= n:
            total_area += int(sl[2])
    return total_area


def find_coverage(info):
    return int(info.split("DP=")[-1].split(";")[0])


def normalized_editing(sites, total_area_covered):
    return round((sites * 1.0e6) / total_area_covered, 5)


def resolve_names(names, default_groups):
    """

    :param names:
    :param default_groups:
    :return:
    """
    if names:
        group_names = names[0].split(",")
    else:
        group_names = [str(group_int) for group_int in range(len(default_groups))]
    return group_names


def sample(parser):
    """ Normalize the editing rates among samples based on the entire sample.

    chromosome  depth   bps_covered_by  percent
    1   0   218256120  248956422    0.876684
    1   1   9532255    248956422    0.0382889
    1   2   4797353    248956422    0.0192699
    1   3   2582708    248956422    0.0103741

    hekase sanorm --names    SRR2087305,SRR1998058,SRR2087291                          \\
                  --vcf      SRR2087305.vcf SRR1998058.vcf SRR2087291.vcf              \\
                  --coverage SRR2087305.cov.tsv SRR1998058.cov.tsv SRR2087291.cov.tsv  \\

    """

    r_script = """

    library(lme4)
    library(lmerTest)
    library(stringr)

    df1 <- list()

    df1$Counts      <- Counts
    df1$Area        <- Area
    df1$Coverage    <- Coverage

    df1$id        <- id
    df1$group     <- group_id

    df1$Area             <-log(df1$Area)
    df1$area_squared     <-df1$Area**2
    df1$coverage_squared <-df1$Coverage**2
    df1$area_cubed       <-df1$Area**3

    # lm3me <- lmer(Counts ~ Area + areaSq + areaCub + Coverage + Coverage:Area + Area*type+(1|id), data=df1)

    lm3me <- lmer(Counts ~ Area + area_squared + area_cubed + Coverage + Coverage:Area + (1|id), data=df1)

    coef1 <- as.matrix(coef(lm3me)$id[1,])

    df1$pred <- mean(coef(lm3me)$id[,1]) +
                coef1[,'Area'] * df1$Area +
                coef1[,'area_squared'] * df1$area_squared +
                coef1[,'area_cubed'] * df1$area_cubed +
                coef1[,'Coverage'] * df1$Coverage +
                coef1[,'Area:Coverage'] * df1$Area * df1$Coverage

    preds <- df1$pred

    ############# Statistical test #############   lmer anova

    df1$norm <- df1$pred - df1$Counts
    norm <- df1$norm

    lm4me <- update(lm3me,.~.+group)

    # print(lm4me)
    # lm4me_summary <- summary(lm4me)

    lm4me_anova <- anova(lm4me)

    p_value <- lm4me_anova["Pr(>F)"][[1]][[5]]

    """

    import rpy2.robjects as robjects
    import numpy as np
    from rpy2.robjects import Formula
    from rpy2.robjects import Environment
    from rpy2.robjects import StrVector, IntVector, FloatVector
    # from rpy2 import robjects
    # from rpy2.robjects import lme4
    # import rpy2's package module
    import rpy2.robjects.packages as rpackages
    # lme4 = rpackages.importr('lme4')
    lme = rpackages.importr('lmerTest')
    from lib.shared import determine_aggregation_depth

    shared_params(parser, gtf=False, bed=True)

    args = parser.parse_args()

    assert len(args.coverage) == len(args.vcf)

    vcf_groups = args.vcf
    coverage_groups = args.coverage
    vcf_groups = build_grouped_sample_data_structure(vcf_groups)
    coverage_groups = build_grouped_sample_data_structure(coverage_groups)

    # =============================================================================================
    # Determine depth to begin aggregation.
    # After a certain depth more reads will negligibly increase the change of detecting editing.
    # =============================================================================================
    min_coverage = int(args.min_coverage)
    min_editing = int(args.min_editing)
    max_cov = float(args.max_editing)
    max_depth = determine_aggregation_depth(min_editing)

    max_cl = 30

    # Decide what the group names should be.
    group_names = resolve_names(args.names, vcf_groups)

    tmp_list = [group_int for group_int in range(len(vcf_groups))]

    # =========================================================================
    # Make SampleGroups Obj
    # =========================================================================

    sample_groups_obj = SampleGroup(min_editing=min_editing, min_coverage=min_coverage, max_percent_coverage=max_cov)

    for group_name_i in range(len(group_names)):

        sample_groups_obj.add_group(
            group_name=group_names[group_name_i],
            vcf_file_list=vcf_groups[group_name_i],
            coverage_file_list=coverage_groups[group_name_i],
            alignment_file_list=None
        )

    # =============================================================================================
    # Compare Groups
    # =============================================================================================
    p_value = ""
    max_val = 50
    coverage_bins_all = list(sorted(sample_groups_obj.get_coverage_bins()))
    coverage_bins = []
    for cba in coverage_bins_all:
        if cba < max_val:
            coverage_bins.append(cba)

    titles = [
        "group_1_name",
        "group_2_name",
        "ECPM_1",
        "ECPM_2",
        "p-val"
    ]
    print("\t".join(titles))
    # remove largest values
    for group_1_name, group_2_name in sample_groups_obj.get_group_comparison_tuples():

        tmp_id_list = []
        tmp_group_names = []
        tmp_exp_type_list = []
        tmp_es_cnt = []
        tmp_area_covered = []
        tmp_coverage_level = []

        avg_group_ecpm = {}
        for group_name in [group_1_name, group_2_name]:

            # Get group vals group_name
            group_average_vals = []

            for sample_name in sample_groups_obj.get_samples_in_group(group_name):
                sample_id = sample_name.get_file_name()

                tmp_avg_ecpm = []
                tmp_avg_cov = []
                for coverage_level in coverage_bins:
                    area_covered_at_level = sample_name.get_area_covered_by_n_reads(coverage_level)
                    editing_sites_at_level = sample_name.get_editing_sites_covered_by_n_reads(coverage_level)

                    if coverage_level < max_cl:
                        tmp_avg_ecpm.append( (editing_sites_at_level*1e6)/area_covered_at_level  )
                        tmp_avg_cov.append(area_covered_at_level)

                    tmp_id_list.append(sample_id)
                    tmp_group_names.append(group_name)
                    tmp_exp_type_list.append(group_name)

                    tmp_es_cnt.append(editing_sites_at_level)
                    tmp_area_covered.append(area_covered_at_level)
                    tmp_coverage_level.append(coverage_level)
                try:
                    group_average_vals.append(np.average(tmp_avg_ecpm, weights=tmp_avg_cov))
                except:
                    group_average_vals.append(-1.0)
            avg_group_ecpm[group_name] = np.average(group_average_vals)

        # ====================================================================
        #                     Add data to R environment.
        # ====================================================================

        robjects.globalenv['id'] = StrVector(tmp_id_list)
        robjects.globalenv['group_id'] = StrVector(tmp_group_names)
        robjects.globalenv['exp_type_list'] = StrVector(tmp_exp_type_list)
        robjects.globalenv['Counts'] = FloatVector(tmp_es_cnt)
        robjects.globalenv['Area'] = FloatVector(tmp_area_covered)
        robjects.globalenv['Coverage'] = FloatVector(tmp_coverage_level)

        # ====================================================================
        #                     Evaluate statistical test
        # ====================================================================
        robjects.r(r_script)

        p_value = robjects.globalenv["p_value"][-1]

        out_list = [
            group_1_name,
            group_2_name,
            str(round(avg_group_ecpm[group_1_name], 5)),
            str(round(avg_group_ecpm[group_2_name], 5)),
            str(round(p_value, 5))
        ]

        print("\t".join(out_list))


def filter_bases(bases, min_coverage=5):
    """

    :param bases:
    :param min_coverage:
    :param alt_cnt:
    :return:
    """
    group_over_cutoff = []
    group_over_cutoff_alt = []
    total_coverage = 0
    for tmp_sample_name in bases:
        ref_cnt = float(bases[tmp_sample_name][0])
        alt_cnt = float(bases[tmp_sample_name][1])
        total = ref_cnt + alt_cnt
        if total >= min_coverage:  # and alt_cnt >= min_editing
            total_coverage += total
            group_over_cutoff.append(alt_cnt / total)
            group_over_cutoff_alt.append(alt_cnt)

    return group_over_cutoff, total_coverage


def decide_outcome(p_value_1, group_1, p_value_2, group_2, cutoff=0.05):

    if p_value_1 <= 0.05:
        return p_value_1, group_1
    elif p_value_2 <= 0.05:
        return p_value_2, group_2
    else:
        return sorted([p_value_1, p_value_2])[0], "NONS"


def generate_group_pairs(group_list):
    # Make Comparison Groups
    group_pair_list = []

    tmp_list = list(sorted(group_list))
    while tmp_list:
        name_1 = tmp_list.pop(0)
        for name_2 in tmp_list:
            group_pair_list.append((name_1, name_2))

    return group_pair_list


def region_diff(parser):
    """ Check for significant differences in editing within genomic regions among samples.

    :param parser:
    :return:
    """

    from lib.parsers import BED
    from scipy.stats import ttest_ind
    import rpy2.robjects as robjects
    from rpy2.robjects import StrVector, FloatVector
    import rpy2.robjects.packages as rpackages

    from lib.parsers  import EditingInSample
    # from import generate_snvs, coverage_depth, find_numbers_of_ref_and_alt_reads
    r_script = """

    df1<-data.frame(
       group=group_ids,
       sampleEPK=sample_epk,
       regionEPK=region_epk,
       regionSize=region_size,
       regionDepth=region_depth
    )

    minVal <- min( df1$regionEPK[df1$regionEPK>0] ) / 2
    df1$regionEPK[df1$regionEPK<=0] <- minVal
    df1$logRegionEPK <-log(df1$regionEPK)
    df1$logSampleEPK <-log(df1$sampleEPK)

    lm1<-lm(logRegionEPK ~ logSampleEPK + regionSize + regionDepth + group, data=df1)

    p_value <- summary(lm1)$coefficients

    """

    parser.add_argument(
        "--regions",
        type=str,
        help="")

    parser.add_argument(
        "--names",
        nargs='+',
        type=str,
        help="")

    parser.add_argument(
        "--sample-epk",
        nargs='+',
        type=str,
        help="")

    parser.add_argument(
        "--region-epk",
        nargs='+',
        type=str,
        help="")

    args = parser.parse_args()

    bed_path = args.regions

    sample_epk = build_grouped_sample_data_structure(args.sample_epk)
    region_epk = build_grouped_sample_data_structure(args.region_epk)

    # max_cl = 30
    # Decide what the group names should be.
    # Returns a list of group names.
    # Will be a list of comma separated names provided to the names parameter or a list of integers.
    group_names = resolve_names(args.names, sample_epk)

    tmp_list = [group_int for group_int in range(len(sample_epk))]

    # =========================================================================
    # Make SampleGroups Obj
    # =========================================================================

    groups_dict = {}
    for group_i in range(len(group_names)):
        for sample_i in range(len(sample_epk[group_i])):

            epk_in_sample = sample_epk[group_i][sample_i]
            epk_in_region = region_epk[group_i][sample_i]

            editing_obj = EditingInSample(epk_in_sample, epk_in_region)

            tmp_group_name = group_names[group_i]
            try:
                groups_dict[tmp_group_name].append(editing_obj)
            except KeyError:
                groups_dict[tmp_group_name] = [editing_obj]

    # Get a a tuple of all possible sets of two groups.
    group_comparisons = generate_group_pairs(group_names)

    bed_obj = BED(bed_path)
    rpvalcnt = 0
    tpvalcnt = 0
    testable = 0
    for record in bed_obj.yield_lines():

        for group_1_name, group_2_name in group_comparisons:

            region_epks, sample_epks, region_size, region_avg_depth, group_ids = [], [], [], [], []
            val_dict = {}

            for tmp_group_name in (group_1_name, group_2_name):
                for tmp_sample in groups_dict[tmp_group_name]:

                    group_ids.append(tmp_group_name)
                    sample_epks.append(tmp_sample.get_sample_epk())

                    # Region-wise data.
                    tmp_region_epk = tmp_sample.get_region_epk(record.name)
                    region_epks.append(tmp_sample.get_region_epk(record.name))
                    region_size.append(tmp_sample.get_region_size(record.name))
                    region_avg_depth.append(tmp_sample.get_region_depth(record.name))

                    try:
                        val_dict[tmp_group_name].append(tmp_region_epk)
                    except KeyError:
                        val_dict[tmp_group_name] = [tmp_region_epk]

            cov_cutoff = 0.75
            min_editing_area = 15
            max_depth_cov = 0.75
            if sum(region_epks) > 0:

                area_cov = stdev(region_size)/mean(region_size)
                depth_cov = stdev(region_avg_depth)/mean(region_avg_depth)

                region_max_editing_area = sorted(region_size)[-1]

                if area_cov < cov_cutoff and min_editing_area < region_max_editing_area and depth_cov < max_depth_cov:

                    testable += 1
                    #print("group_ids", group_ids)
                    #print("sample_epks", sample_epks)
                    #print("region_epks", region_epks)
                    #print("region_size", region_size)
                    #print("region_avg_depth", region_avg_depth)
                    robjects.globalenv['group_ids'] = StrVector(group_ids)
                    robjects.globalenv['region_depth'] = FloatVector(region_avg_depth)
                    robjects.globalenv['sample_epk'] = FloatVector(sample_epks)
                    robjects.globalenv['region_epk'] = FloatVector(region_epks)
                    robjects.globalenv['region_size'] = FloatVector(region_size)


                    robjects.r(r_script)

                    p_value = robjects.globalenv["p_value"][-1]

                    if p_value < 0.05:
                        rpvalcnt += 1

                    g1_mean = mean(val_dict[group_1_name])
                    g2_mean = mean(val_dict[group_2_name])

                    ttest_results = ttest_ind(val_dict[group_1_name], val_dict[group_2_name])
                    if ttest_results[1] < 0.05:
                        tpvalcnt += 1

                    print(
                        "\t".join(
                         [
                             group_1_name,
                             group_2_name,
                             record.name,
                             record.chromosome+":"+str(record.start)+"-"+str(record.end),
                             str(round(g1_mean, 2)),
                             str(round(g2_mean, 2)),
                             str(round(p_value, 7)),
                             str(round(ttest_results[1], 7))
                          ]
                        )
                    )

    print(rpvalcnt)
    print(tpvalcnt)
    print(testable)
    print("COV Cutoff:", cov_cutoff)
    print("lm",    rpvalcnt/float(testable))
    print("ttest", tpvalcnt/float(testable))

'''
            larger_hypothesis = []
            smaller_hypothesis = []
            no_difference = []
            data_matrix = []
            pval_list_group_1_larger = []
            pval_list_group_2_larger = []
            pval_weight = []
            brownable = True
            last_value = None

            # Generate comparison lists.


            group_1_bases = sample_groups_obj.get_region_epk(record.id)
            group_2_bases = sample_groups_obj.get_region_epk(record.id)

            # Is depth similar?
            depth_testable = True
            # Is coverage similar?
            coverage_testable = True

            len(pval_list_group_1_larger),
            len(pval_list_group_2_larger),
            round(mean(pval_weight)),

            if depth_testable and coverage_testable:
                test_obj = ttest_ind(equal_var=True)
            else:
                out_list = [group_1_name, group_2_name, "NO_TEST"]
    '''



"""
            for position in count_char_position_set:

                # Get editing data covered by sites in group 1
                # Get editing data covered by sites in group 2

                # Get coverage at sites
                group_1_bases = sample_groups_obj.get_coverage_at_site_for_group(group_1_name, record.chromosome, position, ref_char, alt_char)
                group_2_bases = sample_groups_obj.get_coverage_at_site_for_group(group_2_name, record.chromosome, position, ref_char, alt_char)

                # Make sure that all sites have minimum read coverage.
                group_1_over_cutoff, total_cov_1 = filter_bases(group_1_bases, min_coverage=min_coverage)
                group_2_over_cutoff, total_cov_2 = filter_bases(group_2_bases, min_coverage=min_coverage)

                pval = None
                SIG = ""
                max_pval = 0.05

                if len(group_2_over_cutoff) >= 2 and len(group_1_over_cutoff) >= 2:

                    ttest_obj = ttest_ind(group_1_over_cutoff, group_2_over_cutoff, equal_var=True)  # False

                    site_average = (total_cov_1 + total_cov_2) / float(len(group_1_over_cutoff) + len(group_2_over_cutoff))
                    current_len = len(group_2_over_cutoff) + len(group_1_over_cutoff)
                    if last_value is not None and current_len != last_value:
                         brownable = False
                    last_value = current_len

                    data_matrix.append(group_1_over_cutoff+group_2_over_cutoff)

                    test_stat = ttest_obj.statistic
                    p_val = ttest_obj.pvalue

                    g1_larger_pval = 0
                    g2_larger_pval = 0
                    if np.isnan(p_val):
                        no_difference.append(1.0)
                        g1_larger_pval = 0.9999
                        g2_larger_pval = 0.9999
                    else:
                        pval_weight.append(site_average)

                        if test_stat > 0:
                            larger_hypothesis.append(p_val)
                            g1_larger_pval = p_val
                            g2_larger_pval = 1 - p_val
                        else:
                            smaller_hypothesis.append(p_val)
                            g1_larger_pval = 1 - p_val
                            g2_larger_pval = p_val

                        pval_list_group_1_larger.append(g1_larger_pval)
                        pval_list_group_2_larger.append(g2_larger_pval)

            if len(pval_list_group_1_larger) >= 2:

                if brownable:

                    data_matrix = np.transpose(data_matrix)

                    ebm_g1_larger = EmpiricalBrownsMethod(data_matrix, pval_list_group_1_larger, extra_info=False)
                    ebm_g2_larger = EmpiricalBrownsMethod(data_matrix, pval_list_group_2_larger, extra_info=False)

                    cp1 = ebm_g1_larger
                    cp2 = ebm_g2_larger

                else:
                    cp1 = combine_pvalues(pval_list_group_1_larger, method='stouffer', weights=pval_weight)[-1]
                    cp2 = combine_pvalues(pval_list_group_2_larger, method='stouffer', weights=pval_weight)[-1]

                d_pval, d_group = decide_outcome(cp1, group_1_name, cp2, group_2_name, cutoff=0.05)

                print(
                    record.chromosome, record.start, record.end,  record.end - record.start, record.name,
                    group_1_name, group_2_name,
                    len(pval_list_group_1_larger),
                    len(pval_list_group_2_larger),
                    round(mean(pval_weight)),
                    d_pval,
                    d_group,
                    brownable,
                    # larger_group,
                    # test_desc,
                    # out_pval,
                    # ebm_g1_larger,
                    # ebm_g2_larger,
                    # combine_pvalues(pval_list_group_1_larger)[-1],  # , method='stouffer'
                    # combine_pvalues(pval_list_group_2_larger)[-1]   # , method='stouffer'
                )
"""


def editing_site_diff(parser):
    """ Check for significant differences in editing of individual sites among samples.

    Sample_A1,Sample_A2  Sample_B1,Sample_B2  Sample_C1,Sample_C2

    """

    from lib.parsers import BED
    from scipy.stats import ttest_ind
    import rpy2.robjects as robjects
    from rpy2.robjects import StrVector, FloatVector
    # import rpy2.robjects.packages as rpackages
    from lib.parsers import EditingInSample

    # from import generate_snvs, coverage_depth, find_numbers_of_ref_and_alt_reads
    r_script = """

    df1<-data.frame(
       group=group_ids,
       sampleEPK=sample_epk,
       regionEPK=region_epk,
       regionSize=region_size,
       regionDepth=region_depth
    )

    minVal <- min( df1$regionEPK[df1$regionEPK>0] ) / 2
    df1$regionEPK[df1$regionEPK<=0] <- minVal
    df1$logRegionEPK <-log(df1$regionEPK)
    df1$logSampleEPK <-log(df1$sampleEPK)

    lm1<-lm(logRegionEPK ~ logSampleEPK + regionSize + regionDepth + group, data=df1)

    p_value <- summary(lm1)$coefficients

    """

    # shared_params(parser, gtf=False, editing_islands=False, bed=True, genome=True)

    parser.add_argument(
        "--names",
        nargs='+',
        type=str,
        help="")

    parser.add_argument(
        "--sample-epk",
        nargs='+',
        type=str,
        help="")

    parser.add_argument(
        "--vcf",
        nargs='+',
        type=str,
        help="")

    args = parser.parse_args()


    # alignment_groups = args.alignment
    # vcf_groups = args.vcf
    # coverage_groups = args.coverage

    sample_epk = build_grouped_sample_data_structure(args.sample_epk)
    vcf_edited = build_grouped_sample_data_structure(args.vcf)

    # vcf_groups = build_grouped_sample_data_structure(vcf_groups)
    # coverage_groups = build_grouped_sample_data_structure(coverage_groups)
    # alignment_groups = build_grouped_sample_data_structure(alignment_groups)

    # min_coverage = int(args.min_coverage)
    # min_editing = int(args.min_editing)
    # max_cov = float(args.max_editing)
    # max_depth = determine_aggregation_depth(min_editing)
    # reference_genome = Fastafile(args.genome)

    # max_cl = 30

    # Decide what the group names should be.
    # Returns a list of group names.
    # Will be a list of comma separated names provided to the names parameter or a list of integers.
    group_names = resolve_names(args.names, sample_epk)

    tmp_list = [group_int for group_int in range(len(sample_epk))]

    # =========================================================================
    # Make SampleGroups Obj
    # =========================================================================

    groups_dict = {}
    for group_i in range(len(group_names)):
        for sample_i in range(len(sample_epk[group_i])):

            epk_in_sample = sample_epk[group_i][sample_i]
            epk_in_site = vcf_edited[group_i][sample_i]

            editing_obj = EditingInSample(epk_in_sample, region_epk_file=None, vcf_file=epk_in_site)

            tmp_group_name = group_names[group_i]
            try:
                groups_dict[tmp_group_name].append(editing_obj)
            except KeyError:
                groups_dict[tmp_group_name] = [editing_obj]

    group_comparisons = generate_group_pairs(group_names)

    # bed_obj = BED(bed_path)

    name_list = {}
    for tmp_group in group_comparisons:
        for tmp_sample in groups_dict[tmp_group_name]:
            for tmp_record_name, tmp_record in tmp_sample.yield_editing_sites():
                try:
                    name_list[tmp_record_name] += 1
                except KeyError:
                    name_list[tmp_record_name] = 1

    rpvalcnt = 0
    tpvalcnt = 0
    testable = 0
    for record in name_list:

        for group_1_name, group_2_name in group_comparisons:
            region_epks = []
            sample_epks = []
            region_size = []
            region_avg_depth = []
            group_ids = []
            val_dict = {}

            for tmp_group_name in (group_1_name, group_2_name):

                if tmp_group_name not in val_dict:
                    val_dict[tmp_group_name] = []

                for tmp_sample in groups_dict[tmp_group_name]:

                    tmp_avg_depth = tmp_sample.get_site_depth(record)
                    if tmp_avg_depth > 0:
                        region_avg_depth.append(tmp_avg_depth)

                        group_ids.append(tmp_group_name)
                        sample_epks.append(tmp_sample.get_sample_epk())
                        region_size.append(1.0)

                        # Region-wise data.
                        tmp_region_epk = tmp_sample.get_site_epk(record)
                        region_epks.append(tmp_region_epk)
                        val_dict[tmp_group_name].append(tmp_region_epk)

            if len(val_dict[group_1_name]) > 2 and len(val_dict[group_2_name]) > 2:
                cov_cutoff = 0.75
                min_editing_area = 15
                max_depth_cov = 0.75
                if sum(region_epks) > 0:
                    area_cov = stdev(region_size) / mean(region_size)
                    depth_cov = stdev(region_avg_depth) / mean(region_avg_depth)
                    region_max_editing_area = sorted(region_size)[-1]
                    if area_cov < cov_cutoff and depth_cov < max_depth_cov:
                        testable += 1
                        #print(record, area_cov, depth_cov)
                        #print("group_ids", group_ids)
                        #print("sample_epks", sample_epks)
                        #print("region_epks", region_epks)
                        #print("region_size", region_size)
                        #print("region_avg_depth", region_avg_depth)

                        robjects.globalenv['group_ids'] = StrVector(group_ids)
                        robjects.globalenv['sample_epk'] = FloatVector(sample_epks)

                        robjects.globalenv['region_epk'] = FloatVector(region_epks)
                        robjects.globalenv['region_size'] = FloatVector(region_size)
                        robjects.globalenv['region_depth'] = FloatVector(region_avg_depth)

                        robjects.r(r_script)

                        p_value = robjects.globalenv["p_value"][-1]

                        # print(p_value)
                        if p_value < 0.05:
                            rpvalcnt += 1

                        #print(val_dict[group_1_name], val_dict[group_2_name])
                        ttest_results = ttest_ind(val_dict[group_1_name], val_dict[group_2_name])
                        #print(ttest_results)
                        if ttest_results[1] < 0.05:
                            tpvalcnt += 1

                        g1_mean = mean(val_dict[group_1_name])
                        g2_mean = mean(val_dict[group_2_name])

                        print(
                            "\t".join(
                                [
                                    group_1_name,
                                    group_2_name,
                                    record,
                                    str(round(g1_mean, 2)),
                                    str(round(g2_mean, 2)),
                                    str(round(p_value, 7)),
                                    str(round(ttest_results[1], 7))
                                ]
                            )
                        )

    print(rpvalcnt)
    print(tpvalcnt)
    print(testable)
    print("COV Cutoff:", cov_cutoff)
    print("lm", rpvalcnt / float(testable))
    print("ttest", tpvalcnt / float(testable))

'''
    # Number of sites allowed for non-editing
    # min sample coverage - must have been detected in n samples.
    shared_params(parser, gtf=False)
    # shared_params(parser, gtf=False, bed=True)

    args = parser.parse_args()

    assert len(args.coverage) == len(args.vcf)

    vcf_groups = args.vcf
    coverage_groups = args.coverage
    alignment_groups = args.alignment

    vcf_groups = build_grouped_sample_data_structure(vcf_groups)
    coverage_groups = build_grouped_sample_data_structure(coverage_groups)
    alignment_groups = build_grouped_sample_data_structure(alignment_groups)

    # =============================================================================================
    # Determine depth to begin aggregation.
    # After a certain depth more reads will negligibly increase the change of detecting editing.
    # =============================================================================================
    min_coverage = int(args.min_coverage)
    min_editing = int(args.min_editing)
    max_cov = float(args.max_editing)
    max_depth = 30
    max_cl = 30

    # Decide what the group names should be.
    group_names = resolve_names(args.names, vcf_groups)
    tmp_list = [group_int for group_int in range(len(vcf_groups))]

    # =========================================================================
    # Make SampleGroups Obj
    # =========================================================================

    sample_groups_obj = SampleGroup(min_editing=min_editing, min_coverage=min_coverage, max_percent_coverage=max_cov)

    for group_name_i in range(len(group_names)):
        sample_groups_obj.add_group(
            group_name=group_names[group_name_i],
            vcf_file_list=vcf_groups[group_name_i],
            coverage_file_list=coverage_groups[group_name_i],
            alignment_file_list=alignment_groups[group_name_i]
        )

    group_tuples = sample_groups_obj.get_group_comparison_tuples()

    # Make a consensus set of all VCF sites.
    for chromosome, position, ref_char, alt_char in sample_groups_obj.get_all_vcf_sites():
        # Run each group test.
        for group_1_name, group_2_name in group_tuples:

            # Get coverage at sites
            group_1_bases = sample_groups_obj.get_coverage_at_site_for_group(
                group_1_name, chromosome, position, ref_char, alt_char)
            group_2_bases = sample_groups_obj.get_coverage_at_site_for_group(
                group_2_name, chromosome, position, ref_char, alt_char)

            testable = False
            group_1_len = len(group_1_bases)
            group_2_len = len(group_2_bases)

            ratios_1 = []
            group_1_over_cutoff = []
            group_1_over_cutoff_alt = []
            for tmp_sample_name in group_1_bases:
                tmp_tuple = group_1_bases[tmp_sample_name]
                ref_cnt = float(tmp_tuple[0])
                alt_cnt = float(tmp_tuple[1])
                total = ref_cnt + alt_cnt
                if total >= min_coverage:  # and alt_cnt > min_editing
                    group_1_over_cutoff.append(alt_cnt/total)
                    group_1_over_cutoff_alt.append(alt_cnt)
                    ratios_1.append(str(alt_cnt)+"|"+str(total))

            ratios_2 = []
            group_2_over_cutoff = []
            group_2_over_cutoff_alt = []

            for tmp_sample_name in group_2_bases:
                tmp_tuple = group_2_bases[tmp_sample_name]
                ref_cnt = float(tmp_tuple[0])
                alt_cnt = float(tmp_tuple[1])
                total = ref_cnt + alt_cnt
                if total >= min_coverage:  # and alt_cnt > min_editing
                    group_2_over_cutoff.append(alt_cnt/total)
                    group_2_over_cutoff_alt.append(alt_cnt)
                    ratios_2.append(str(alt_cnt) + "|" + str(total))

            pval = None
            SIG = ""
            max_pval = 0.05
            # print(group_1_over_cutoff, group_2_over_cutoff, float(group_1_len)/2)
            # if float(group_1_len)/2 >= group_1_len and float(group_2_len)/2 >= group_2_len:
            if len(group_2_over_cutoff) >= 2 and len(group_1_over_cutoff) > 2:
                # print(group_1_over_cutoff, group_2_over_cutoff)
                ttest_obj_1 = ttest_ind(group_1_over_cutoff, group_2_over_cutoff, equal_var=False)
                #ttest_obj_1 = ttest_ind(group_1_over_cutoff, group_2_over_cutoff)
                ttest_obj_2 = ttest_ind(group_1_over_cutoff_alt, group_2_over_cutoff_alt, equal_var=False)
                p1 = ttest_obj_1.pvalue
                # p2 = ttest_obj_2.pvalue
                # pval = sorted([p1, p2])[0]
                # print(p1, p2, pval)
                # print(group_1_over_cutoff, group_2_over_cutoff, pvalue_1)

                if pval <= max_pval:
                    SIG = "SIG"
                else:
                    SIG = "NS"

            else:
                SIG = "NO_TEST"
                pval = 1

            if SIG != "NO_TEST":
                print(chromosome, position, group_1_name, group_2_name, round(pval, 5), SIG, ratios_1, ratios_2)

'''