#!/bin/bash

nretools sample --gtf Homo_sapiens.GRCh38.90.chr1.gtf               \
                --names SRR1,SRR2                                   \
                --vcf   SRR1.editingSites.vcf,SRR2.editingSites.vcf
