from lib.parsers import generate_snvs, coverage_depth, find_numbers_of_ref_and_alt_reads
from lib.io import shared_params, build_grouped_sample_data_structure
from math import factorial
from lib.shared import passes_min_coverage, passes_min_editing, get_strand, base_transition_tuples_and_titles, passes_max_percent_editing
from lib.parsers import GenomicIntervalTree, BedIntervalTree, find_total_area_covered


def choose(n, k):
    return factorial(n) / factorial(k) / factorial(n - k)


def calculate_EPMCBP(parser):
    """
    chromosome  depth   bps_covered_by  percent
    1	0	218256120	248956422	0.876684
    1	1	9532255	248956422	0.0382889
    1	2	4797353	248956422	0.0192699
    1	3	2582708	248956422	0.0103741



    :return:
    """
    parser.add_argument("--coverage",
                        type=str,
                        required=True,
                        help='')

    parser.add_argument("--vcf",
                        type=str,
                        required=True,
                        help='')

    args = parser.parse_args()

    editing_sites = 0
    with open(args.vcf) as vcf_file:
        for line in vcf_file:
            editing_sites += 1

    coverage_dict = {}
    with open(args.coverage) as coverage_file:
        for line in coverage_file:
            sl = line.split()


def normalized_editing(sites, total_area_covered):
    return round((sites * 1.0e6) / total_area_covered, 5)


def calculate_EPCMRGN(parser):
    """ Calculate EPCMBGN - Editing Per Megabase of Mapped Reads with coverage Greater than N.

    samtools view -b {input} | genomeCoverageBed -ibam stdin -g {params.genome} > {output}

    hekase sample --gtf   Homo_sapiens.GRCh38.90.gtf                   \
                  --names SRR2087305,SRR1998058,SRR2087291             \
                  --vcf   SRR2087305.vcf,SRR1998058.vcf,SRR2087291.vcf \
                  --bam   SRR2087305,SRR1998058,SRR2087291

    parser.add_argument("--bam",
                        required=False,
                        help='')

    """

    parser.add_argument("--vcf",
                        type=str,
                        required=True,
                        help='')

    parser.add_argument("--coverage",
                        required=False,
                        help='')

    # Count number of editing sites.
    args = parser.parse_args()

    editing_site_counts = 0
    for chrom, pos, id, ref, alt, qual, fil, info in generate_snvs(args.vcf):
        editing_site_counts += 1

    coverage_dict = {}
    total = 0
    with open(args.coverage) as coverage_file:
        for line in coverage_file:
            sl = line.split()
            if sl[0] != "genome" and int(sl[1]) > 0:
                total += int(sl[2])

    if args.coverage:  # sites, total_area_covered
        normalized_site_count = normalized_editing(editing_site_counts, total)

    print(editing_site_counts, total, normalized_site_count)


def sample(parser):
    """ Normalize the editing rates among samples based on the entire sample.

    chromosome  depth   bps_covered_by  percent
    1   0   218256120  248956422    0.876684
    1   1   9532255    248956422    0.0382889
    1   2   4797353    248956422    0.0192699
    1   3   2582708    248956422    0.0103741

    hekase sanorm --names    SRR2087305,SRR1998058,SRR2087291                          \\
                  --vcf      SRR2087305.vcf SRR1998058.vcf SRR2087291.vcf              \\
                  --coverage SRR2087305.cov.tsv SRR1998058.cov.tsv SRR2087291.cov.tsv  \\

    Weighted norm with dropping out not covered regions.


    """

    shared_params(parser, gtf=False)

    args = parser.parse_args()
    from statistics import mean

    assert len(args.coverage) == len(args.vcf)

    vcf_groups = args.vcf
    coverage_groups = args.coverage

    vcf_groups = build_grouped_sample_data_structure(vcf_groups)
    coverage_groups = build_grouped_sample_data_structure(coverage_groups)

    names = []
    if args.names:
        names = args.names.split(",")

    # =============================================================================================
    # Determine depth to begin aggregation.
    # After a certain depth more reads will negligibly increase the change of detecting editing.
    # =============================================================================================
    min_coverage = int(args.min_coverage)
    min_editing = int(args.min_editing)
    max_depth = 1

    # Calculate the probablity of non-detection
    # probablity of zero through min_editing-1

    def non_detection_prob(p, k):

        p_editing = 0.15
        p_no_editing = 1.0 - p_editing

        prob_sum = (p_no_editing ** p)  # No editing sites are detected
        # prob_sum += (p_editing ** p)    # All sites contain editing sites and therefore are discarded as a SNV.

        for ki in range(1, k):
            prob_sum += choose(p, ki) * (0.1 ** ki) * (0.9 ** (p - ki))
        return prob_sum

    for i in range(min_editing + 1, 5000):
        # prob_const = (0.2 ** min_editing) * (0.8 ** (i - min_editing))
        # choose_val = choose(i, min_editing)
        detection_prob = 1.0 - non_detection_prob(i, min_editing)
        max_depth = i
        # print(max_depth, min_editing, round(detection_prob, 2))

        if detection_prob > 0.99:
            break

    # =============================================================================================
    # Parse the coverage data
    # =============================================================================================
    list_of_coverage_dicts = []
    column_titles = []
    for coverage_file_group_i in range(len(coverage_groups)):

        number_in_group = 0
        for coverage_file_name in coverage_groups[coverage_file_group_i]:
            coverage_area_dict = {}

            if args.names:
                number_in_group += 1
                col_title = "%s_%s" % (names[coverage_file_group_i], number_in_group)
                column_titles.append(col_title)
            else:
                column_titles.append(coverage_file_name)

            with open(coverage_file_name) as coverage_file:
                for line in coverage_file:
                    sl = line.split()
                    coverage_level = int(sl[1])
                    area_coverage = int(sl[2])
                    if sl[0] == "genome" and coverage_level >= min_coverage:

                        coverage_level = max_depth if coverage_level > max_depth else coverage_level

                        try:
                            coverage_area_dict[coverage_level] += area_coverage
                        except KeyError:
                            coverage_area_dict[coverage_level] = area_coverage
            # Add length assertion check here.
            list_of_coverage_dicts.append(coverage_area_dict)

    # =============================================================================================
    # Create matrix of scaling values.
    # =============================================================================================
    scaling_factors = []
    mean_coverage_sum = 0
    for i in range(max_depth+1):
        if i >= min_coverage:
            coverage_values = [s[i] for s in list_of_coverage_dicts]
            m = mean(coverage_values)
            mean_coverage_sum += m
            sf = [cv / m for cv in coverage_values]
        else:
            sf = [0 for s in list_of_coverage_dicts]

        scaling_factors.append(sf)

    # =============================================================================================
    # Parse vcf files into count matrices.
    # =============================================================================================
    site_depth_list = []
    sample_list = []
    group_list = []
    flist = []
    for vcf_file_group in vcf_groups:
        for vcf_file_name in vcf_file_group:
            vcf_cov_dict = {}
            flist.append(vcf_file_name)
            for line in open(vcf_file_name):

                vcf_depth = coverage_depth(line)
                ref_num, alt_numb = find_numbers_of_ref_and_alt_reads(line)

                over_min_editing = passes_min_editing(alt_numb, args.min_editing)
                over_min_coverage = passes_min_coverage(ref_num + alt_numb, args.min_coverage)
                under_max_percent_editing = passes_max_percent_editing(ref_num, alt_numb, 100)

                if over_min_editing and over_min_coverage and under_max_percent_editing:

                    vcf_depth = max_depth if vcf_depth > max_depth else vcf_depth

                    try:
                        vcf_cov_dict[vcf_depth] += 1
                    except KeyError:
                        vcf_cov_dict[vcf_depth] = 1

            site_depth_list.append(vcf_cov_dict)

    # =============================================================================================
    # Scale the counts
    # =============================================================================================

    scaled_sum = []
    for i in range(min_coverage, max_depth):

        counts_at_depth_i = []
        for n in site_depth_list:
            try:
                counts_at_depth_i.append(n[i])
            except KeyError:
                counts_at_depth_i.append(0)

        coverage_at_depth_i = []
        for n in list_of_coverage_dicts:
            try:
                coverage_at_depth_i.append(n[i])
            except KeyError:
                coverage_at_depth_i.append(1)

        editing_rates = [(counts_at_depth_i[e])/float(coverage_at_depth_i[e]) for e in range(len(counts_at_depth_i))]

        # group_list.append(vcf_file_group)
        # sample_list.append(vcf_file_name)
        to_matrix = {}
        zcnts = []
        zcov = []

        for si in range(len(editing_rates)):
            z1 = counts_at_depth_i[si]
            z2 = coverage_at_depth_i[si]
            zcnts.append(str(z1))
            zcov.append(str(z2))
            # print(sample_list[si] + "\t" + "\t".join([str(i), str(counts_at_depth_i[si]), str(coverage_at_depth_i[si])]))

        print(str(i)+"\t"+"\t".join(zcnts) + "\t" + "\t".join(zcov))

        # print("\t".join([str(s) for s in ]))
        # print("\t".join([str(s) for s in coverage_at_depth_i]))
        # print("\t".join([str(s) for s in coverage_at_depth_i]))

        scaling_factors_at_depth_i = [n for n in scaling_factors[i]]

        tmp_list = []
        for j in range(len(counts_at_depth_i)):
            tmp_list.append(counts_at_depth_i[j]/scaling_factors_at_depth_i[j])

        if not scaled_sum:
            scaled_sum = tmp_list
        else:
            scaled_sum = [x + y for x, y in zip(scaled_sum, tmp_list)]

    # EPCM = [(s*10**6)/mean_coverage_sum for s in scaled_sum]
    # print("\t".join(column_titles))
    # print(scaled_sum)
    # print("\t".join([str(round(e, 5)) for e in EPCM]))


def genomic_region(parser):
    """ Normalize the editing rates among samples based on provided genomic regions.

    :param parser:
    :return:
    """
    shared_params(parser, gtf=False)

    args = parser.parse_args()
    from statistics import mean

    assert len(args.coverage) == len(args.vcf)

    vcf_groups = args.vcf
    coverage_groups = args.coverage

    vcf_groups = build_grouped_sample_data_structure(vcf_groups)
    coverage_groups = build_grouped_sample_data_structure(coverage_groups)

    names = []
    if args.names:
        names = args.names.split(",")

    # =============================================================================================
    # Determine depth to begin aggregation.
    # After a certain depth more reads will negligibly increase the change of detecting editing.
    # =============================================================================================
    min_coverage = int(args.min_coverage)
    min_editing = int(args.min_editing)
    max_depth = 1

    # Calculate the probablity of non-detection
    # probablity of zero through min_editing-1



def editing_site(parser):
    """ Normalize the editing rates among samples based on editing sites.

    :param parser:
    :return:
    """
    pass