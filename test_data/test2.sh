#!/bin/bash

python3 hekase.py sstats --gtf      test_data/Homo_sapiens.GRCh38.90.chr1.gtf                           \
                         --names    SRR1636957,SRR1663126                                               \
                         --coverage test_data/SRR1636957.coverage.tsv,test_data/SRR1663126.coverage.tsv \
                         --vcf      test_data/SRR1636957.editingSites.vcf,test_data/SRR1663126.editingSites.vcf
