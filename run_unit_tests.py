import unittest
from lib.parsers import *
from lib.shared import *


class ParserTests(unittest.TestCase):

    def test__find_numbers_of_ref_and_alt_reads_t1(self):
        self.assertTrue(find_numbers_of_ref_and_alt_reads("1/1:0,2:2:6:65,6,0") == (0, 2))

    def test__find_numbers_of_ref_and_alt_reads_t2(self):
        input_string = "ABHom=1.00;AC=2;AF=1.00;BaseCounts=BaseCounts=0,0,2,0;ExcessHet=3.0103;"
        self.assertTrue(find_numbers_of_ref_and_alt_reads(input_string) == (0, 2))


class SharedTests(unittest.TestCase):

    def test__passes_min_coverage__1(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 5
        min_coverage = 5
        self.assertTrue(passes_min_coverage(coverage, min_coverage))

    def test__passes_min_coverage__2(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 10
        min_coverage = 5
        self.assertTrue(passes_min_coverage(coverage, min_coverage))

    def test__passes_min_coverage__3(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        coverage = 2
        min_coverage = 5
        self.assertFalse(passes_min_coverage(coverage, min_coverage))

    """ Should return true if greater than or equal to min_editing.
    """
    def test__passes_min_editing__1(self):
        editing = 5
        min_editing = 5
        self.assertTrue(passes_min_coverage(editing, min_editing))

    def test__passes_min_editing__2(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        editing = 10
        min_editing = 5
        self.assertTrue(passes_min_coverage(editing, min_editing))

    def test__passes_min_editing__3(self):
        """ Should return true if greater than or equal to min_coverage.
        """
        editing = 2
        min_editing = 5
        self.assertFalse(passes_min_coverage(editing, min_editing))

    def test__choose__1(self):
        self.assertTrue(choose(10, 5) == 252)

    def test__determine_aggregation_depth__1(self):
        self.assertTrue(determine_aggregation_depth(2) == 45)

    def test__get_strand__1(self):
        record_strand = "+"
        a_to_g_cnt = 0
        c_to_t_cnt = 0
        self.assertTrue(get_strand(record_strand, a_to_g_cnt, c_to_t_cnt) == "A")

    def test__get_strand__2(self):
        record_strand = "-"
        a_to_g_cnt = 0
        c_to_t_cnt = 0
        self.assertTrue(get_strand(record_strand, a_to_g_cnt, c_to_t_cnt) == "C")

    def test__get_strand__3(self):
        record_strand = "."
        a_to_g_cnt = 10
        c_to_t_cnt = 1
        self.assertTrue(get_strand(record_strand, a_to_g_cnt, c_to_t_cnt) == "A")

    def test__get_strand__4(self):
        record_strand = "."
        a_to_g_cnt = 2
        c_to_t_cnt = 7
        self.assertTrue(get_strand(record_strand, a_to_g_cnt, c_to_t_cnt) == "C")

    def test__base_transition_tuples_and_titles__4(self):

        transition_tuple_titles, transition_tuple_list = base_transition_tuples_and_titles()

        transition_tuple_titles_test = [
            'A>C', 'A>G', 'A>T', 'C>A', 'C>G', 'C>T', 'G>A', 'G>C', 'G>T', 'T>A', 'T>C', 'T>G'
        ]
        transition_tuple_list_test = [
            ('A', 'C'), ('A', 'G'), ('A', 'T'), ('C', 'A'), ('C', 'G'), ('C', 'T'),
            ('G', 'A'), ('G', 'C'), ('G', 'T'), ('T', 'A'), ('T', 'C'), ('T', 'G')
        ]

        self.assertTrue(transition_tuple_titles == transition_tuple_titles_test)
        self.assertTrue(transition_tuple_list == transition_tuple_list_test)


if __name__ == '__main__':
    unittest.main()


